{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise - Natural Pairing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Knowledge](#Knowledge)\n",
    "  * [Modules](#Python-Modules)\n",
    "* [Theory - Background](#Theory---Background)\n",
    "  * [Binary Classification](#Binary-Classification)\n",
    "  * [Backpropagation](#Backpropagation)\n",
    "* [Exercises](#Exercises)\n",
    "  * [Exercise - Partial Derivative of the Activation Function](#Exercise---Partial-Derivative-of-the-Activation-Function)\n",
    "  * [Exercise - Logistic Function with Squared Error](#Exercise---Logistic-Function-with-Squared-Error)\n",
    "  * [Exercise - Logistic Function with Cross-Entropy](#Exercise---Logistic-Function-with-Squared-Error)\n",
    "  * [Exercise - Concrete Example](#Exercise---Concrete-Example)\n",
    "    * [Exercise - Plot Sigmoid and its Derivative](#Exercise---Plot-Sigmoid-and-its-Derivative)\n",
    "    * [Exercise - Plot Squared Error and Cross-entropy](#Exercise---Plot-Squared-Error-and-Cross-entropy)\n",
    "    * [Exercise - Plot Derivatives of Squared Error and Cross-entropy](#Exercise---Plot-Derivatives-of-Squared-Error-and-Cross-entropy)\n",
    "    * [Exercise - Plot the Final Product](#Exercise---Plot-the-Final-Product)\n",
    "  * [Sidenote - Why the Squared Error works for Linear Regression](#Sidenote---Why-the-Squared-Error-works-for-Linear-Regression)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Introduction\n",
    "\n",
    "The cross-entropy is a widely used loss function when we use logistic regression (and also neural networks) for classification tasks. \n",
    "\n",
    "The squared error is commonly used as loss function when we perform linear regression to predict continuous values.\n",
    "\n",
    "Completing this exercise you will see why the squared error does not work for classification tasks:\n",
    "- by looking at the math behind logistic regression\n",
    "- and visually by plotting the individual functions and their derivatives\n",
    "\n",
    "In order to detect errors in your own code, execute the notebook cells containing `assert` or `assert_almost_equal`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "\n",
    "To complete this exercise notebook, you should possess knowledge about the following topics.\n",
    " - Logistic function\n",
    " - Cost functions:\n",
    "  - Cross-entropy\n",
    "  - Squared error\n",
    " - Computational graph\n",
    " - Backpropagation\n",
    "\n",
    "The following material can help you to acquire this knowledge:\n",
    "* Squared error, cross-entropy, computational graph, backpropagation:\n",
    " * Chapter 5 and 6 of the [Deep Learning Book](http://www.deeplearningbook.org/) [GOO16]\n",
    " * Chapter 5 of the book Pattern Recognition and Machine Learning by Christopher M. Bishop [BIS07]\n",
    "* Logistic Regression (binary):\n",
    " * Video 15.3 and following in the playlist [Machine Learning](https://www.youtube.com/watch?v=-Z2a_mzl9LM&list=PLD0F06AA0D2E8FFBA&t=740s&index=110) of the youtube user *mathematicalmonk* [MAT18]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# External Modules\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Theory - Background"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Binary Classification\n",
    "\n",
    "For binary classification the logistic activation function $\\sigma (z) = \\frac{1}{1+\\exp(-z)}$ is used, hence the name *logistic regression*. Sigmoid function is often used as a synonym for the logistic function, even though the logistic function is just a special case of a sigmoid function. The tangens hyperbolicus, e.g. is also a sigmoid function.\n",
    "\n",
    "When we talk about the sigmoid function in this paper, we use it as a synonym for the logistic function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Backpropagation\n",
    "\n",
    "The training process of logistic regression consists of optimizing the weights $\\theta$, so the prediction $\\sigma(z) = \\sigma(x; \\theta)$ gets closer to the desired target (or class) $t$.\n",
    "\n",
    "This is accomplished using the update rule:\n",
    "\n",
    "$$\n",
    "\\theta_i^{new} = \\theta_i^{old} - \\lambda \\frac{\\partial J}{\\partial \\theta_i^{old}}\n",
    "$$\n",
    "\n",
    "$\\lambda$ is the learning rate ($\\lambda > 0$) and the term $\\frac{\\partial J}{\\partial \\theta_i}$ is computed using the backpropagation algorithm. The partial derivative of the loss function $J$ is derived by applying the chain rule:\n",
    "\n",
    "$$\n",
    "\\frac{\\partial J}{\\partial \\theta_i} = \n",
    "\\frac{\\partial J}{\\partial \\sigma} \\frac{\\partial \\sigma}{\\partial z} \\frac{\\partial z}{\\partial \\theta_i}\n",
    "$$\n",
    "\n",
    "The factors are propagated back to compute efficiently the gradient of the loss (point wise cost) with backpropagation.\n",
    "\n",
    "The first factor $\\frac{\\partial J}{\\partial \\sigma}$ depends on the cost function $J$ and the \n",
    "second factor $\\frac{\\partial \\sigma}{\\partial z}$ on the activation function $\\sigma$.\n",
    "\n",
    "The product of the first two factors are backpropagated in the network."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise - Partial Derivative of the Activation Function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Pen & Paper exercise**\n",
    "\n",
    "Since we want to compare the *sigmoid function + squared loss* with the *logistic function + cross-entropy*, the second term $\\frac{\\partial \\sigma}{\\partial z}$ stays the same for both.\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Compute $\\frac{\\partial \\sigma(z)}{\\partial z}$ for the sigmoid activation function $\\sigma (z) = \\frac{1}{1+\\exp(-z)}$\n",
    "and express the result as a function of $\\sigma(z)$:\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "The final solution is $\\frac{\\partial \\sigma(z)}{\\partial z} = \\ldots = \\sigma(z) (1-\\sigma(z))$. The task is to find out what is behind the \"$\\ldots$\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise - Sigmoid Function with Squared Error"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Pen & Paper exercise**\n",
    "\n",
    "**Task:**\n",
    "\n",
    "1. What is the product of the first two factors ($\\frac{\\partial J}{\\partial \\sigma} \\frac{\\partial \\sigma}{\\partial z} $) if we use the squared error as lost (cost of one example):\n",
    "$$\n",
    "J(\\sigma) = \\frac{1}{2} (\\sigma(z)-t)^2\n",
    "$$\n",
    " with \n",
    "  * the target $t \\in \\{0,1\\}$ (classification).\n",
    "  *  $\\sigma(z)$ is interpreted as the predicted probability of class 1: $p(y=1 \\mid \\vec x)$.\n",
    "  \n",
    "  \n",
    "2. Why is the squared error problematic?\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "1. First compute $\\frac{\\partial J}{\\partial \\sigma}$ as done in the first exercise. Then compute the product $\\frac{\\partial J}{\\partial \\sigma} \\frac{\\partial \\sigma}{\\partial z} $.\n",
    "2. When you do not know how to solve this task, insert some valid values for $t$ and $z$. If you still cannot answere this question, skip it, proceed with this notebook until you have finished all of the plotting-exercises (which visualize this problem), then come back to answere this question."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise - Sigmoid Function wit Cross-entropy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Pen & Paper exercise**\n",
    "\n",
    "**Task:**\n",
    "\n",
    "1. What is the product of the first two factors ($\\frac{\\partial J}{\\partial \\sigma} \\frac{\\partial \\sigma}{\\partial z} $) if we use the cross-entropy as lost (cost of one example):\n",
    "$$\n",
    "J(\\sigma) = - t \\log(\\sigma(z)) - (1-t) \\log (1-\\sigma(z))\n",
    "$$\n",
    " with \n",
    "  * the target $t \\in \\{0,1\\}$ (classification).\n",
    "  *  $\\sigma(z)$ is interpreted as the predicted probability of class 1: $p(y=1 \\mid \\vec x)$.\n",
    "\n",
    "\n",
    "2. Why is there no such problem for the cross-entropy loss?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise - Concrete Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "To visualize the theory of this exercise we will now plot the functions for concrete values. Use the results of the pen & paper exercises to implement your methods."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Exercise - Plot Sigmoid and its Derivative\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Implement both functions and execute the code cell for the plot.\n",
    " - the sigmoid function $\\sigma(z)$\n",
    " - the derivative of the sigmoid $\\frac{\\partial \\sigma(z)}{\\partial z}$ to see the steepest point of the sigmoid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "z = np.linspace(-8,8,200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Implement these functions\n",
    "\n",
    "def sigmoid(z):\n",
    "    \"\"\"\n",
    "    Returns the sigmoid of z.\n",
    "    \n",
    "    :z: concrete values.\n",
    "    :z type: 1D numpy array of type float32.\n",
    "    \n",
    "    :returns: sigmoid of z.\n",
    "    :r type: 1D numpy array of type float32\n",
    "            with the same length as z.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()\n",
    "    \n",
    "def derivative_sigmoid(z):\n",
    "    \"\"\"\n",
    "    Returns the derivative of sigmoid of z.\n",
    "    \n",
    "    :z: concrete values.\n",
    "    :z type: 1D numpy array of type float32.\n",
    "    \n",
    "    :returns: derivative of sigmoid of z.\n",
    "    :r type: 1D numpy array of type float32\n",
    "            with the same length as z.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Execute to verify your implementation\n",
    "\n",
    "np.testing.assert_almost_equal(sigmoid(-100), 0)\n",
    "np.testing.assert_almost_equal(sigmoid(100), 1)\n",
    "assert derivative_sigmoid(0) == 0.25"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Execute this if you have implemented the functions above\n",
    "\n",
    "plt.plot(z, sigmoid(z), label=\"sigmoid\", color=\"blue\")\n",
    "plt.plot(z, derivative_sigmoid(z), label=\"derivative_sigmoid\", color=\"black\")\n",
    "plt.legend()\n",
    "plt.xlabel(\"z\")\n",
    "plt.ylabel(\"$\\sigma(z)$ resp. $\\sigma'(z)$\")\n",
    "_ = plt.title(\"sigmoid\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Exercise - Plot Squared Error and Cross-entropy\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Now implement the squared error and the cross-entropy for concrete $\\sigma$ and $t=1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "sigma = np.linspace(0.01,1,200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Implement these functions\n",
    "\n",
    "def squared_error(sigma):\n",
    "    \"\"\"\n",
    "    Returns the suqared error of sigma for class t=1.\n",
    "    \n",
    "    :sigma: concrete value between 0 and 1\n",
    "    :sgima type: float32.\n",
    "    \n",
    "    :returns: suqared error of sigma for class t=1.\n",
    "    :r type: float32.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()\n",
    "\n",
    "def cross_entropy(sigma):\n",
    "    \"\"\"\n",
    "    Returns the suqared error of sigma for class t=1.\n",
    "    \n",
    "    :sigma: concrete value between 0 and 1\n",
    "    :sgima type: float32.\n",
    "    \n",
    "    :returns: suqared error of sigma for class t=1.\n",
    "    :r type: float32.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Execute to verify your implementation\n",
    "\n",
    "assert squared_error(1) == 0\n",
    "assert cross_entropy(1) == 0\n",
    "np.testing.assert_almost_equal(squared_error(0.1), 0.405)\n",
    "np.testing.assert_almost_equal(cross_entropy(0.1), 2.3025850929940455)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Execute this if you have implemented the functions above\n",
    "\n",
    "plt.plot(sigma, squared_error(sigma), label=\"squared_error\", color=\"red\")\n",
    "plt.plot(sigma, cross_entropy(sigma), label=\"cross_entropy\", color=\"green\")\n",
    "plt.xlabel(\"sigma\")\n",
    "plt.ylabel(\"loss\")\n",
    "plt.legend()\n",
    "_ = plt.title(\"loss for $t=1$\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Exercise - Plot Derivatives of Squared Error and Cross-entropy\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Now implement the derivative of the loss $\\frac{\\partial J(\\sigma)}{\\partial \\sigma}$ for\n",
    "- the squared error \n",
    "- the cross-entropy \n",
    "\n",
    "for concrete $\\sigma$ and $t=1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Implement these functions\n",
    "\n",
    "def derivative_squared_error(sigma):\n",
    "    \"\"\"\n",
    "    Returns the derivative of the suqared error of \n",
    "    sigma for class t=1.\n",
    "    \n",
    "    :sigma: concrete value between 0 and 1\n",
    "    :sgima type: float32.\n",
    "    \n",
    "    :returns: derivative of squared error of sigma for class t=1.\n",
    "    :r type: float32.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()\n",
    "\n",
    "def derivative_cross_entropy(sigma):\n",
    "    \"\"\"\n",
    "    Returns the derivative of the  suqared error of \n",
    "    sigma for class t=1.\n",
    "    \n",
    "    :sigma: concrete value between 0 and 1\n",
    "    :sgima type: float32.\n",
    "    \n",
    "    :returns: derivative of suqared error of sigma for class t=1.\n",
    "    :r type: float32.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "assert derivative_squared_error(0) == -1\n",
    "assert derivative_cross_entropy(0.01) == -100\n",
    "assert derivative_squared_error(1) == 0\n",
    "assert derivative_cross_entropy(1) == -1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "plt.plot(sigma, derivative_squared_error(sigma), label=\"derivative squared_error\", color=\"red\")\n",
    "plt.plot(sigma, derivative_cross_entropy(sigma), label=\"derivative cross_entropy\", color=\"green\") \n",
    "plt.xlabel(\"sigma\")\n",
    "plt.ylabel(\"derivative loss\")\n",
    "plt.ylim(-10,1)\n",
    "plt.legend()\n",
    "_ = plt.title(\"derivative of the loss for $t=1$\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Exercise - Plot the Final Product\n",
    "\n",
    "\n",
    "**Task:**\n",
    "\n",
    "And finally implement the functions for the product $\\frac{\\partial J}{\\partial \\sigma}\\frac{\\partial \\sigma}{\\partial z}$ for\n",
    "- the squared error \n",
    "- the cross-entropy \n",
    "\n",
    "for concrete $\\sigma$ and $t=1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def product_derivative_squared_error_and_derivative_sigmoid(sigma):\n",
    "    \"\"\"\n",
    "    Returns the product of the derivative of \n",
    "    the suqared error and the derivative of \n",
    "    the sigmoid for concrete sigma and class t=1.\n",
    "    \n",
    "    :sigma: concrete value between 0 and 1\n",
    "    :sgima type: float32.\n",
    "    \n",
    "    :returns: see above.\n",
    "    :r type: float32.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()\n",
    "\n",
    "def product_derivative_cross_entropy_and_derivative_sigmoid(sigma):\n",
    "    \"\"\"\n",
    "    Returns the product of the derivative of \n",
    "    the cross-entropy and the derivative of \n",
    "    the sigmoid for concrete sigma and class t=1.\n",
    "    \n",
    "    :sigma: concrete value between 0 and 1\n",
    "    :sgima type: float32.\n",
    "    \n",
    "    :returns: see above.\n",
    "    :r type: float32.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "assert product_derivative_cross_entropy_and_derivative_sigmoid(0) == -1\n",
    "assert product_derivative_squared_error_and_derivative_sigmoid(0) == 0\n",
    "assert product_derivative_cross_entropy_and_derivative_sigmoid(1) == 0\n",
    "assert product_derivative_squared_error_and_derivative_sigmoid(1) == 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "plt.plot(sigma, product_derivative_squared_error_and_derivative_sigmoid(sigma), label=\"derivative squared_error\", color=\"red\")\n",
    "plt.plot(sigma, product_derivative_cross_entropy_and_derivative_sigmoid(sigma), label=\"derivative cross_entropy\", color=\"green\") \n",
    "plt.xlabel(\"sigma\")\n",
    "plt.ylabel(\"derivative loss\")\n",
    "plt.legend()\n",
    "_ = plt.title(\"derivative of the loss for $t=1$\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Sidenote - Why the Squared Error works for Linear Regression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "For linear regression our activation function $a$ is the identity function. Or in other words, there is no activation function.\n",
    "\n",
    "This means $a(z) = z$ and therefor $\\frac{\\partial a(z)}{\\partial z} =1$, which results in:\n",
    "\n",
    "$$\n",
    "\\frac{\\partial J}{\\partial a}\\frac{\\partial a}{\\partial z} = \\frac{\\partial J}{\\partial a} 1 = \\frac{\\partial J}{\\partial a}\n",
    "$$\n",
    "\n",
    "\n",
    "\n",
    "When you completed the exercises above, then you will see that the term for linear regression $\\frac{\\partial J}{\\partial a}$ (with squared error) exactly equals the product term for logistic regression (with crossentropy):\n",
    "\n",
    "$$\n",
    "\\frac{\\partial J}{\\partial a}= \\frac{\\partial J}{\\partial \\sigma} \\frac{\\partial \\sigma}{\\partial z}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "        <tr>\n",
    "        <td>\n",
    "            <a name=\"BIS07\"></a>[BIS07]\n",
    "        </td>\n",
    "        <td>\n",
    "            Christopher M. Bishop, Pattern recognition and machine learning, 5th Edition. Springer 2007, ISBN 9780387310732.\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"GOO16\"></a>[GOO16]\n",
    "        </td>\n",
    "        <td>\n",
    "            Goodfellow, Ian, et al. Deep learning. Vol. 1. Cambridge: MIT press, 2016.\n",
    "        </td>\n",
    "    </tr>\n",
    "        <tr>\n",
    "        <td>\n",
    "            <a name=\"MAT18\"></a>[MAT18]\n",
    "        </td>\n",
    "        <td>\n",
    "            mathematicalmonk. (2018, September 30). (ML 15.3) Logistic regression (binary) - intuition. And following in the playlist Machine Learning. Retrieved from https://www.youtube.com/watch?v=-Z2a_mzl9LM&list=PLD0F06AA0D2E8FFBA&t=740s&index=110\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "Exercise - Natural Pairing <br/>\n",
    "by Christian Herta, Klaus Strohmenger<br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Christian Herta, Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
